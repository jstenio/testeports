package com.testeports;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.awt.event.MouseEvent;

public class GluonApplication extends Application {

    /*public static void main(String[] args) {
        launch(args);
    }*/

    @Override
    public void start(Stage stage) {
        //StackPane root = new StackPane(new Label("Hello JavaFx Mobile"));
        Group root = new Group();
        Button btn = new Button("Teste Button");
        btn.setOnAction((e)->stage.setFullScreen(true));

        Label label = new Label("Teste root label");
        Rectangle rectangle = new Rectangle(100, 100, 100, 100);
        rectangle.setFill(Color.BLUE);

        label.setAlignment(Pos.CENTER);
        label.setLayoutX(100);
        label.setLayoutY(200);

        root.getChildren().addAll(rectangle, btn, label);
        Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
        Scene scene = new Scene(root, bounds.getWidth(), bounds.getHeight(), Color.RED);
        scene.setOnMouseClicked((e)->{
            System.out.println(e.getSceneX());
            System.out.println(e.getScreenX());
            System.out.println(e.getX());
            label.setText("Teste rooot label 1");
            rectangle.setLayoutX(e.getX());
            rectangle.setLayoutY(e.getY());
        });
        stage.getIcons().add(new Image(GluonApplication.class.getResourceAsStream("/icon.png")));
        stage.setTitle("Teste Ports");
        stage.setScene(scene);
        stage.show();
    }
}
